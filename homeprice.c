#include <stdio.h>

int main()
{
    FILE *f;
    int zip, id, price, numBeds, numBaths, area;
    int maxPrice = 0;
    int minPrice = 1000000000;
    int homeCounter = 0;
    long long totalPrice = 0;
    int averagePrice = 0;
    char address[40];
    
    f = fopen("homelistings.csv", "r");
    
    // make sure the file opens
    if (!f)
    {
        printf("ERROR: Could not open the file.\n");
        return 0;
    }
    
    // read in the file
    while (scanf("%d, %d, %[^,], %d, %d, %d, %d", &zip, &id, address, &price, &numBeds, &numBaths, &area) != EOF)
    {
        // check lowest price
        if (price < minPrice)
            minPrice = price;
        // check highest price
        if (price > maxPrice)
            maxPrice = price;
        
        totalPrice += price;
        homeCounter++;
    }
    
    // calculates the overall average
    averagePrice = totalPrice / homeCounter;
    printf("%d, %d, %d\n", minPrice, maxPrice, averagePrice);
    
    // close the file
    fclose(f);
    return 0;
}
