#include <stdio.h>

int main()
{
    FILE *f;   // declare the file
    int zip, id, price, numBeds, numBaths, area;
    char address[40];
    int input = 0;

    f = fopen("homelistings.csv", "r");   // read it in

    // make sure the file opens
    if (!f)
    {
        printf("ERROR: Could not open the file.\n");
        return 0;
    }
    
    printf("[\n");
    while (scanf("%d, %d, %[^,], %d, %d, %d, %d", &zip, &id, address, &price, &numBeds, &numBaths, &area) != EOF)
    {
        if (input)
            printf(",\n");
        else 
            input = 1;
        printf("{ \"zip\": %d, \"address\": \"%s\", \"price\": %d, \"area\": %d}", zip, address, price, area);
    }
    printf("\n]");

    fclose(f);
    return 0;
}
