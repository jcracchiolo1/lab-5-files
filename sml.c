#include <stdio.h>

int main()
{
    //FILE *f;
    int zip, id, price, numBeds, numBaths, area;
    char address[40];
    
    FILE *f = fopen("homelistings.csv", "r");
    FILE *fSmall = fopen("small.txt", "w");
    FILE *fMed = fopen("med.txt", "w");
    FILE *fLarge = fopen("large.txt", "w");
    
    // make sure the file opens
    if (!f)
    {
        printf("ERROR: Could not open the file.\n");
        return 0;
    }
    
    // read in the file
    while (scanf("%d, %d, %[^,], %d, %d, %d, %d", &zip, &id, address, &price, &numBeds, &numBaths, &area) != EOF)
    {
        if (area < 1000)
            fprintf(fSmall, "%s : %d\n", address, area);
            
        else if (area > 2000)
            fprintf(fLarge, "%s : %d\n", address, area); 
            
        else 
            fprintf(fMed, "%s : %d\n", address, area);       
    }
    
    
    // close the file
    fclose(f);
    return 0;
}
