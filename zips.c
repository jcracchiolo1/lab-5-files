#include <stdio.h>

int main()
{
    int zip, id, price, numBeds, numBaths, area;
    char address[40];
    char fname[20];
    
    FILE *f = fopen("homelistings.csv", "r");
    FILE *temp;   // temperay file to open and close
    
    // make sure the file opens
    if (!f)
    {
        printf("ERROR: Could not open the file.\n");
        return 0;
    }
    
    // read in the file
    while (scanf("%d, %d, %[^,], %d, %d, %d, %d", &zip, &id, address, &price, &numBeds, &numBaths, &area) != EOF)
    {
        sprintf(fname, "%d.txt", zip);   // creates a string with the name of the file to open
        temp = fopen(fname, "a");        // opens that file
        
        if (!temp)                       // make sure that file opens
        {
            printf("ERROR: Could not open the file.\n");
            return 0;
        }
        
        fprintf(temp, "%s\n", address);  // prints to that file 
        fclose(temp);                    // then close that file 
        
    }
        
    // close the file
    fclose(f);
    return 0;
}
